﻿using System;

namespace friday_morning
{
    class Student
    {
        string Name;
       
        int ID;

        string Address02;


        string Address {get; set;}

        public string GetAddress

        {

            get {return Address02;
            }
        }
        private string SetAddress
        {
            set {
                Address02 = value;
            }
        }

        public Student (string _name, int _idnumber)

        {
            Name = _name;
            ID = _idnumber;
        }
        public void SayHello()
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine($"Hello my name is {Name} and I am a Student At Toi Ohomai and my Student ID number is {ID}");
        }
    
        class Program
        {
            static void Main(string[] args)
            {
            var p1 = new Student("Ian", 10013028);
            p1.SayHello();

            p1.Address = "43HarrierrStreet";
            Console.WriteLine(p1.GetAddress);
            }
        }           
        }
    }